#!/bin/env

INPUT="ugkcyxxp"
#INPUT="abc"

import hashlib
import re

def crack_pw(input_base):
    password_digits = []
    idx = 0
    
    fivezeros = re.compile("""^00000""")
    
    while len(password_digits) < 8:
        tohash = "{0}{1}".format(input_base, idx)
        
        # compute md5sum of tohash
        # if hashed starts with 00000
        # password_digits.append(hashed[5]) # 6th digit
        
        m = hashlib.md5()
        m.update(tohash)
        hashed = m.hexdigest()
        
        #matched = fivezeros.search(hashed)
        five = hashed[:5]
        matched = (five == "00000")
        if matched:
            nextchar = hashed[5]
            password_digits.append(nextchar)
        
        idx += 1
        
    return "".join(password_digits)

def crack_pw_2(input_base):
    password_digits = [-1] * 8
    discovered = 0
    #discovered_idx = [3231929, 5357525, 5708769, 8036669, 8605828, 8609554, 13666005, 13753421]
    idx = 0
    
    while discovered < 8:
        tohash = "{0}{1}".format(input_base, idx)
        
        m = hashlib.md5()
        m.update(tohash)
        hashed = m.hexdigest()
        
        five = hashed[:5]
        if five == "00000":
            try:
                nextpos = int(hashed[5]) # ValueError
                if nextpos < 8 and password_digits[nextpos] == -1:
                    nextchar = hashed[6]
                    password_digits[nextpos] = nextchar
                    discovered += 1
                    #discovered_idx.append(idx)
            except ValueError:
                pass # ignore invalid ints
        
        idx += 1
    
    return "".join(password_digits)

def main():
    #print crack_pw(INPUT)
    print crack_pw_2(INPUT)

if __name__ == '__main__':
    main()