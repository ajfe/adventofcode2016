#!/bin/env python
import re

INPUT_FILE="day3_input.txt"

class Triangle:
    def __init__(self, side1_len, side2_len, side3_len):
        self.sides = (side1_len, side2_len, side3_len)
        
    def is_possible(self):
        for combi in [(0, 1, 2), (1, 2, 0), (2, 0, 1)]:
            if (self.sides[combi[0]] + self.sides[combi[1]] <= self.sides[combi[2]]):
                return False
        return True
    
    @classmethod
    def parse(cls, inputfile):
        
        reg = re.compile(r"(?P<side1>\d+)\s+(?P<side2>\d+)\s+(?P<side3>\d+)")

        triangles = []
        wip = [[],[],[]] # 3 in-progress triangle x 3 sides
        with open(inputfile) as f:
            lines = f.readlines()
            linenum = 1
            for line in lines:
                m = reg.search(line)
                if m:
                    side1 = int(m.group("side1"))
                    side2 = int(m.group("side2"))
                    side3 = int(m.group("side3"))
                    
                    wip[0].append(side1)
                    wip[1].append(side2)
                    wip[2].append(side3)
                   
                    if linenum == 3: # 3 triangles ready
                        for tri in wip:
                            triangles.append(Triangle(tri[0], tri[1], tri[2]))
                        # reset
                        linenum = 1
                        wip = [[],[],[]]
                    else:
                        linenum += 1
                
        return triangles
                
                

def main():
    triangles = Triangle.parse(INPUT_FILE)
    
    possible = 0
    for tri in triangles:
        if tri.is_possible():
            possible += 1
            
    print possible

if __name__ == '__main__':
    main()