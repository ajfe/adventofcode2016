#!/bin/env python
import re
from _collections import defaultdict

INPUT_FILE="day4_input.txt"

# aaaaa-bbb-z-y-x-123[abxyz]
class Code:
    def __init__(self, hashed, sector_id, checksum):
        self.hashed = hashed
        self.sector_id = int(sector_id)
        self.checksum = checksum
        self.decoded = None

    def __str__(self):
        name = self.hashed
        if self.decoded is not None:
            name = self.decoded
        return "{0}: {1}".format(name, self.sector_id)

    def compute_checksum(self):
        letters = self.hashed.replace("-", "")
        
        # count letter frequency
        letmap = {}
        for l in letters:
            if l in letmap:
                letmap[l] += 1
            else:
                letmap[l] = 1
                
        ## from { a : 1, b : 3, c : 1 } to { 1 : [a, c], 3 : [b] } 
        counts = defaultdict(list)
        for letter, occurrences in letmap.iteritems():
            counts[occurrences].append(letter)
            
        top5freqs = sorted(counts.keys(), reverse=True)[:5]
        almostready = ""
        for freq in top5freqs:
            letters = "".join(sorted(counts[freq]))
            almostready += letters
            
        computed = almostready[:5] # only 5
            
        return computed

    def verify(self):
        ck = self.compute_checksum()
        return ck == self.checksum

#     def rot_alpha(self, n):
#         from string import ascii_lowercase as lc, ascii_uppercase as uc
#         lookup = str.maketrans(lc + uc, lc[n:] + lc[:n] + uc[n:] + uc[:n])
#         return lambda s: s.translate(lookup)

    def rotn(self, original_str, n):
        
            
        def lookup(v, n):
            from string import ascii_lowercase as lc, ascii_uppercase as uc
            chars = lc
            base_char = 'a'
            end_char = 'z'
            if v.isupper():
                chars = uc
                base_char = 'A'
                end_char = 'Z'
            
            base_i = ord(base_char)
            min_c = ord(base_char)
            max_c = ord(end_char)

            if ord(v) < min_c or ord(v) > max_c: #non-alphabetic
                return v
                
            v_i = ord(v) - base_i # translate to index in uc or lc lists
            trans_i = (v_i + n) % len(chars) # rotn and modulo list length
            return chars[trans_i]
            
        rotd = ''.join([ lookup(ch, n) for ch in original_str ])
        return rotd
    
    def decode_room(self):
        dec = self.rotn(self.hashed, self.sector_id)
        self.decoded = dec.replace("-", " ")
        return self.decoded
        
    @classmethod
    def parse(cls, inputfile):
        
        reg = re.compile(r"(?P<hashed>.+)\-(?P<sector_id>\d+)\[(?P<checksum>.+)\]")

        codes = []
        with open(inputfile) as f:
            lines = f.readlines()
            for line in lines:
                m = reg.search(line)
                if m:
                    hashed = m.group("hashed")
                    sector_id = m.group("sector_id")
                    checksum = m.group("checksum")
                    
                    codes.append(Code(hashed, sector_id, checksum))

                
        return codes
                
                

def main():
    codes = Code.parse(INPUT_FILE)
    
    sum_ting = 0
    for c in codes:
        if c.verify():
            sum_ting += c.sector_id
            
        c.decode_room()
        if "north" in c.decoded:
            print c 
            
    print sum_ting

if __name__ == '__main__':
    main()