#!/bin/env python


INPUT="R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5"
#INPUT="R8, R4, R4, R8"

class Step:
    
    def __init__(self, turn, walksteps):
        self.turn = turn
        self.walksteps = walksteps
        
    def __str__(self):
        return "{0}/{1}".format(self.turn, self.walksteps)
        
    @classmethod
    def parse(cls, steps_str):
        
        steps_list = []
        
        steps_str = steps_str.replace(" ", "")
        steps = steps_str.split(",")
        for i in steps:
            turn = i[0]
            walkn = int(i[1:])
            steps_list.append(Step(turn, walkn))
        
        return steps_list

class Pos:
    
    shifts_map = { "N" : (0, 1),
                   "S" : (0, -1),
                   "E" : (1, 0),
                   "W" : (-1, 0) }
    
    turns_map = { "N" : { "R" : "E", "L" : "W" },
                  "S" : { "R" : "W", "L" : "E" },
                  "E" : { "R" : "S", "L" : "N" },
                  "W" : { "R" : "N", "L" : "S" } }
    
    def __init__(self, x, y, direction):
        self.x = x
        self.y = y
        self.direction = direction
    
    def __eq__(self, pos):
        return self.x == pos.x and self.y == pos.y
    
    def __hash__(self):
        return hash((self.x, self.y))
    
    def __str__(self):
        return "({0}, {1}, {2})".format(self.x, self.y, self.direction)
        
    def clone(self):
        return Pos(self.x, self.y, self.direction)
        
    def add(self, tup):
        self.x = self.x + tup[0]
        self.y = self.y + tup[1]
        
    def turn(self, side):
        new_direction = Pos.turns_map[self.direction][side]
        self.direction = new_direction
        
    def walk(self, steps):
        d = self.direction
        shift = Pos.shifts_map[d]
        shift_mul = (shift[0] * steps, shift[1] * steps)
        self.add(shift_mul)


class Grid:
    def __init__(self):
        self.startpos = Pos(0, 0, "N")
        self.currpos = Pos(0, 0, "N")

        self.firstvisited_twice = None
    
    @classmethod
    def distance(cls, endpos, startpos):
        diff_x = abs(endpos.x - startpos.x)
        diff_y = abs(endpos.y - startpos.y)
        return diff_x + diff_y
    
    def shortest_distance(self):
        return Grid.distance(self.startpos, self.currpos)
    
    def do_the_walk(self, steps):
        visited_positions = []
        stepn = 0
        
        for step in steps:
            stepn += 1
         
            self.currpos.turn(step.turn)
            
            for i in xrange(1, step.walksteps + 1):
                self.currpos.walk(1)
                
                print "{0}:{1} {2} {3}".format(stepn, i, self.currpos, step)
                
                if self.firstvisited_twice is None:
                    curr = self.currpos.clone()
                    if self.currpos in visited_positions:
                        self.firstvisited_twice = curr
                    else:
                        visited_positions.append(curr)

def main():
    steps = Step.parse(INPUT)
    
    g = Grid()
    g.do_the_walk(steps)
    distance = g.shortest_distance()
    
    print "Distance: {0}".format(distance)
    
    fvt = g.firstvisited_twice
    if fvt is not None:
        dist = g.distance(fvt, g.startpos)
        print "Distance to first-visited-twice ({1}): {0}".format(dist, fvt)
    else:
        print "No point visited twice"

if __name__ == '__main__':
    main()