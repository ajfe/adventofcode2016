#!/bin/env python

import re

#INPUT_FILE="day11_input_sample.txt"
INPUT_FILE="day11_input.txt"


class Pof:
    @classmethod
    def parse(cls, inputfile):
        
        def ordinal_to_int(ordinal):
            pah = {"first" : 1,
                   "second" : 2,
                   "third" : 3,
                   "fourth" : 4,
                   "fifth" : 5,
                   "sixth" : 6,
                   "seventh" : 7,
                   "eighth" : 8,
                   "ninth" : 9,
                   "tenth" : 10}
            
            return pah[ordinal.lower()]
   
        # The first floor contains a strontium generator, a strontium-compatible microchip, a plutonium generator, and a plutonium-compatible microchip.
        # The fourth floor contains nothing relevant.
        re_floor = re.compile("""The (?P<floorord>.+) floor contains (?P<contents>.+)""")
        re_contents_generator = re.compile("""a (?P<elementname>.+) generator""")
        re_contents_microchip = re.compile("""a (?P<elementname>.+)-compatible microchip""")
        re_contents_nothing = re.compile("""nothing relevant""")
        
        floors = {}
        
        with open(inputfile) as f:
            lines = f.readlines()
            for line in lines:
                line = line.rstrip()
                
                match_floor = re_floor.search(line)
                if match_floor is not None:
                    floorname = ordinal_to_int(match_floor.group("floorord"))
                    floorcontents_str = match_floor.group("contents")
                    
                    floorcontents = []
                    
                    match_nothing = re_contents_nothing.search(floorcontents_str)
                    if match_nothing is None:
                        # remove empty strings introduced by split without oxford comma ("x, and y" vs. "x and y")
                        contents_list = [ sss for sss in re.split(",| and ", floorcontents_str) if sss != "" ] 
                        
                        for content_str in contents_list:
                            match_microchip = re_contents_microchip.search(content_str)
                            if match_microchip is not None:
                                elementname = match_microchip.group("elementname")
                                floorcontents.append("microchip {0}".format(elementname))
                            else:
                                match_generator = re_contents_generator.search(content_str)
                                if match_generator is not None:
                                    elementname = match_generator.group("elementname")
                                    floorcontents.append("generator {0}".format(elementname))
                                else:
                                    raise ValueError("Invalid instruction in line {0}: '{1}'".format(line, content_str)) 
                    else:
                        pass
                    
                    floors[floorname] = floorcontents
                    
        return floors
        
        
def main():
    floors = Pof.parse(INPUT_FILE)
    
    for k, v in floors.iteritems():
        print "{0}: {1}".format(k, v)


if __name__ == '__main__':
    main()