#!/bin/env

import re

INPUT_FILE="day7_input.txt"

class IPv7:
    def __init__(self, addrparts, netparts):
        self.addrparts = addrparts
        self.netparts = netparts
        
    def __str__(self):
        return "SSL={5}; TLS={4}; A({2})={0}; N({3})={1}".format(".".join(self.addrparts), ".".join(self.netparts), len(self.addrparts), len(self.netparts), self.support_tls(), self.support_ssl())

    def support_tls(self):

        abba_in_addr = any([ IPv7.is_abba(ap) for ap in self.addrparts ])
        abba_in_nets = any([ IPv7.is_abba(np) for np in self.netparts ])

        return abba_in_addr and not abba_in_nets
            
    def support_ssl(self):
        # find all ABAs
        # generate BABs from ABAs
        # search all netparts for BABs
        all_abas = [ IPv7.find_aba(ap) for ap in self.addrparts ]
        all_abas = [item for sublist in all_abas for item in sublist] #WTF!!
        
        if len(all_abas) == 0:
            return False
        
        all_babs = [ IPv7.aba_to_bab(aba) for aba in all_abas ]
        
#         any_bab = [ any([ bab in np for np in self.netparts ]) for bab in all_babs ] # mutha fuckin' nonsense
#         if not any_bab:
#             return False
#         else:
#             return True
        for bab in all_babs:
            bab_in_netparts = any([ bab in np for np in self.netparts ])
            if bab_in_netparts:
                return True
            
        return False
             

    @classmethod
    def find_aba(cls, some_string):
        
        abas = []
        while len(some_string) >= 3:
            three = some_string[:3]
            if three[0] == three[2] and three[0] != three[1]:
                abas.append(three)

            some_string = some_string[1:]
                
        return abas
    
    @classmethod
    def aba_to_bab(cls, aba):
        return "{0}{1}{2}".format(aba[1], aba[0], aba[1])

    @classmethod
    def is_abba(cls, some_string):
        while len(some_string) >= 4:
            # grab first 4 characters
            # check if 'ABBA' pattern
            # if not, pop first, repeat
            four = some_string[:4]
            if four[0] != four[1] and (four[0] == four[3] and four[1] == four[2]):
                return True
            else:
                some_string = some_string[1:]
            
        return False
    
    @classmethod
    def parse(cls, inputfile):
        
        # wysextplwqpvipxdv[srzvtwbfzqtspxnethm]syqbzgtboxxzpwr[kljvjjkjyojzrstfgrw]obdhcczonzvbfby[svotajtpttohxsh]cooktbyumlpxostt

        ipv7_addresses = []
        with open(inputfile) as f:
            lines = f.readlines()
            for line in lines:
                line = line.rstrip()

                parts = re.split("\[|\]", line)
                addrparts = parts[::2] # even-numbered indices
                netparts = parts[1::2] # odd-numbered indices
                    
                ipv7_addresses.append(IPv7(addrparts, netparts))

                
        return ipv7_addresses

def main():
    addresses = IPv7.parse(INPUT_FILE)
    
#     for a in addresses:
#         print a
    
    print "Num. supporting TLS: {0}".format(len([ a for a in addresses if a.support_tls() ]))
    print "Num. supporting SSL: {0}".format(len([ a for a in addresses if a.support_ssl() ]))

if __name__ == '__main__':
    main()