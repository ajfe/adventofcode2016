#!/bin/env python

import re
from __builtin__ import True

INPUT_FILE="day10_input.txt"

# class Piece:
#     def __init__(self, piece_id, output_low, output_high, curr_chip=None):
#         self.id = piece_id
#         self.output_low = output_low
#         self.output_high = output_high
#         self.curr_chip = curr_chip
# 
# class Output(Piece):
#     pass

class Bot:
    def __init__(self, botid, botlow, bothigh):
        self.botid = botid #"bot 1123"
        self.botlow = botlow
        self.bothigh = bothigh
        self.chips = []
        
    def add_chip(self, chipn):
        self.chips.append(chipn)
        
    def has_chip(self):
        return len(self.chips) != 0
    
    def remove_chip(self, chipn):
        self.chips.remove(chipn)
        
    def hands_full(self):
        return len(self.chips) == 2
    
    def get_chip(self, whichone):
        chips = sorted(self.chips)
        if whichone == "low":
            return chips[0]
        else: # "high"
            return chips[-1]
    
    def giveto(self, bot, chipn):
        if bot.hands_full():
            return False
        else:
            bot.add_chip(chipn)
            self.remove_chip(chipn)
            return True
    
    def dispatch(self):
        if self.hands_full():
            lowchip = self.get_chip("low")
            highchip = self.get_chip("high")
            
            if lowchip == 17 and highchip == 61:
                print "Smee, {0}".format(self.botid)
            
            dispatched_low = self.giveto(self.botlow, lowchip)
            dispatched_high = self.giveto(self.bothigh, highchip)
            return dispatched_high and dispatched_low
        else:
            return False

        
    def __str__(self):
        return "Bot {0} chip {3} -> ({1},{2})".format(str(self.botid), str(self.botlow.botid), str(self.bothigh.botid), str(self.chips))

class Output(Bot):
    def __init__(self, botid):
        self.botid = botid
        self.chips = []

    def __str__(self):
        return "Output {0} chip {1}".format(str(self.botid), str(self.chips))

class Instruction:
    def __init__(self, frombot, tobot, chipnum):
        self.frombot = frombot
        self.tobot = tobot
        self.chipnum = chipnum

class BotGraph:
    def __init__(self):
        self.bots = {}
        self.outputs = {}
        self.instructions = []
        
    def __str__(self):
        return "### bots ###\n{0}\n### outp ###\n{1}\n### inst ###\n{2}".format("\n".join([ str(bot) for bot in self.bots.itervalues() ]),
                                                   "\n".join([ "{0}: {1}".format(outn, chipn) for outn, chipn in self.outputs.iteritems() ]),
                                                   "{0} instructions pending".format(len(self.instructions)))
    
    def get_idle_bots(self):
        return [ bot for bot in self.bots.itervalues() if bot.hands_full() ]
    
    def is_output_id(self, someid):
        return len(someid) >= 6 and someid[:6] == "output"

    def is_bot_id(self, someid):
        return len(someid) >= 3 and someid[:3] == "bot"
                         
    def has_output(self, outputid):
        return outputid in self.outputs.keys()
    
    def get_output(self, outputid):
        if self.has_output(outputid):
            return self.outputs[outputid]
        else:
            return None
        
    def add_output(self, output):
        self.outputs[output.botid] = output
    
    def has_bot(self, botid):
        return botid in self.bots.keys()
    
    def get_bot(self, botid):
        if self.has_bot(botid):
            return self.bots[botid]
        else:
            return None
    
    def add_bot(self, bot):
        self.bots[bot.botid] = bot
        
    # if theid is not in graph already, create a Bot or Output object and return, else just return 
    def fetch_with_register(self, theid):
        if self.is_bot_id(theid):
            if self.has_bot(theid):
                return self.get_bot(theid)
            else:
                newbot = Bot(theid, None, None)
                self.add_bot(newbot)
                return newbot
        elif self.is_output_id(theid):
            if self.has_output(theid):
                return self.get_output(theid)
            else:
                newoutput = Output(theid)
                self.add_output(newoutput)
                return newoutput
        
    def add_instruction(self, instr):
        self.instructions.append(instr)
        
    def apply_instructions(self):
        for i in self.instructions:
            
            if i.frombot is not None: # move chip
                frombot = self.get_bot(i.frombot)
                tobot = self.get_bot(i.tobot)
                
                # TODO: doesn't make sense
                if frombot.hands_full() and not tobot.hands_full():
                    thechip = frombot.chip
                    frombot.remove_chip()
                    tobot.add_chip(thechip)
            else: # add chip
                tobot = self.get_bot(i.tobot)
                tobot.add_chip(i.chipnum)
                
    def work(self):
        idle_bots = self.get_idle_bots()
        while len(idle_bots) != 0:
            workerer = idle_bots.pop()
            workerer.dispatch()
            idle_bots = self.get_idle_bots()
                
    @classmethod
    def parse(cls, inputfile):
#         value 5 goes to bot 2
#         bot 2 gives low to bot 1 and high to bot 0     
#         bot 107 gives low to output 9 and high to bot 161
   
        re_value = re.compile("""value (?P<chipnum>\d+) goes to bot (?P<botnum>\d+)""")
        re_bot = re.compile("""bot (?P<botnum>\d+) gives low to (?P<outlow>.+) and high to (?P<outhigh>.+)""")
        
        botgraph = BotGraph()
        
        with open(inputfile) as f:
            lines = f.readlines()
            for line in lines:
                line = line.rstrip()
                
                match_instruction_bot = re_bot.search(line)
                if match_instruction_bot is not None:
                    bot_id = "bot {0}".format(match_instruction_bot.group("botnum"))
                    outlow_id = match_instruction_bot.group("outlow")
                    outhigh_id = match_instruction_bot.group("outhigh")
                    
                    outlow = botgraph.fetch_with_register(outlow_id)
                    outhigh = botgraph.fetch_with_register(outhigh_id)
                    
                    bot = botgraph.fetch_with_register(bot_id)
                    
                    bot.botlow = outlow
                    bot.bothigh = outhigh
                    
                else:
                    match_instruction_value = re_value.search(line)
                    if match_instruction_value is not None:
                        chipnum = int(match_instruction_value.group("chipnum"))
                        tobot = "bot {0}".format(match_instruction_value.group("botnum"))
                        instruction = Instruction(frombot=None, tobot=tobot, chipnum=chipnum)
                        botgraph.add_instruction(instruction)
                    else:
                        raise ValueError("Bad instruction '{0}'".format(line)) 


        return botgraph

def main():
    bg = BotGraph.parse(INPUT_FILE)
    bg.apply_instructions()
#     print bg
    bg.work()
    print bg

if __name__ == '__main__':
    main()