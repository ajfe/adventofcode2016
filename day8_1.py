#!/bin/env python

import re

INPUT_FILE="day8_input.txt"

class Operation:
    def __init__(self, action, details):
        self.action = action
        self.details = details # dict { k : v, ... }
    
    def __str__(self):
        return "action={0}; {1}".format(self.action, "; ".join([ "{0}={1}".format(p, v) for p, v in self.details.iteritems() ]))
        
    def is_rect(self):
        return self.action == "rect"
    
    def is_rotate(self):
        return self.action == "rotate"
    
    def is_rotate_row(self):
        return self.is_rotate() and self.details["axis"] == "row"
    
    def is_rotate_column(self):
        return self.is_rotate() and self.details["axis"] == "column"
        
    @classmethod
    def parse(cls, inputfile):
                
        operations = []
        
#         rect 3x2
#         rotate column x=1 by 1
#         rotate row y=0 by 4


        with open(inputfile) as f:
            lines = f.readlines()
            for line in lines:
                line = line.rstrip()

                parts = re.split(" ", line)
                
                instruction = parts[0]
                op_details = None
                
                if instruction == "rect":
                    dim = parts[1]
                    
                    sizing = dim.split("x")                    
                    op_details = { "width" : int(sizing[0]), 
                                   "height" : int(sizing[1]) }
                    
                elif instruction == "rotate":
                    axis = parts[1] # "row" or "column"
                    which = parts[2] # "x=1234"
                    howmuch = int(parts[4]) # 123
                    
                    whichparts = which.split("=")
                    whichn = int(whichparts[1])
                    
                    op_details = { "axis" : axis,
                                   "which" : whichn,
                                   "howmuch" : howmuch,
                                   "direction" : 1 } # move "up in axis" 
                    
                else:
                    raise ValueError("invalid instruction in line {0}".format(line))

                op = Operation(instruction, op_details)
                operations.append(op)
                
        return operations

class Screen:
    def __init__(self, res_x, res_y):
        self.res_x = res_x
        self.res_y = res_y
        self.grid = [ [ 0 for j in range(res_y) ] for i in range(res_x) ] # array of columns
        
    def __str__(self):
        rows_grid = [ [ "" for j in range(self.res_x) ] for i in range(self.res_y) ] # array of rows
        for x in xrange(self.res_x):
            for y in xrange(self.res_y):
                pixvalue = self.grid[x][y]
                pix = self.pix_to_str(pixvalue)
                rows_grid[y][x] = pix
                
        grid_str = "\n".join([ "".join(row) for row in rows_grid] )
        return grid_str

    def num_lit(self):
        return sum([ pix for col in self.grid for pix in col ])
                
    def pix_to_str(self, pixvalue):
        if pixvalue == 0:
            return "."
        else:
            return "#"
            
    def togglepix(self, col, row): # index is 0-based
        x = col
        y = row
        pixvalue = self.grid[x][y]
        if pixvalue == 0:
            self.grid[x][y] = 1
        else:
            self.grid[x][y] = 0

    def apply_operations(self, operations):
        for op in operations:
            self.apply_operation(op)

    def apply_operation(self, op):
        d = op.details
        if op.is_rect():
            self.rect(d["width"], d["height"])
        elif op.is_rotate_row():
            self.rot_row(d["which"], d["howmuch"])
        elif op.is_rotate_column():
            self.rot_column(d["which"], d["howmuch"])
        else:
            raise ValueError("Bad operation? {0}".format(op))

    # shift with wrap-around, specify negative numbers to shift backwards
    def _shift(self, l, n):
        n = -n % len(l)
        return l[n:] + l[:n]

    def rect(self, width, height):
        # turn on pix from x=0 to width-1
        #  and y=0 to height-1
        for x in range(width):
            for y in range(height):
                self.grid[x][y] = 1
    
    def rot_row(self, row_n, by_howmuch):
        # shift all elements in row n by howmuch, wrap-around the edge
        rowvalues = [ col[row_n] for col in self.grid ]
        newrow = self._shift(rowvalues, by_howmuch)
        for col_n in range(len(self.grid)):
            col = self.grid[col_n]
            col[row_n] = newrow[col_n]
            

    def rot_column(self, col_n, by_howmuch):
        # shift all elements in column n by howmuch, wrap-around the egge
        newcol = self._shift(self.grid[col_n], by_howmuch)
        self.grid[col_n] = newcol

def main():
    screen = Screen(50, 6)

    ops = Operation.parse(INPUT_FILE)
#     ops = [ Operation("rect", {"width" : 1, "height" : 4 }),
#             Operation("rect", {"width" : 6, "height" : 2 }), ]

    screen.apply_operations(ops)
    
    print screen
    
    print "Num lit pixels: {0}".format(screen.num_lit())

if __name__ == '__main__':
    main()