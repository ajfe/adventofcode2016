#!/bin/env python

INPUT_FILE="day6_input.txt"

def parse(inputfile):

    columns = []
    
#             letmap = {}
#         for l in letters:
#             if l in letmap:
#                 letmap[l] += 1
#             else:
#                 letmap[l] = 1
    
    with open(inputfile) as f:
        lines = f.readlines()
        for line in lines:
            line = line.rstrip()
            num_cols = len(line)
            
            for col in range(0, num_cols): # col is 0-based index
                if len(columns) <= col: # col=4, len=4, add 5th column
                    columns.append([])
                    
                columns[col].append(line[col])
                
    return columns      

def confabulate(coldata, mostfreq=True):
    confabmaps = []
    for column in coldata:
        
        letmap = {} # { a : 1, f : 3, g : 12 }
        
        for letter in column:
            if letter in letmap:
                letmap[letter] += 1
            else:
                letmap[letter] = 1
            
        confabmaps.append(letmap)
    
    frequentest = []    
    for fab in confabmaps:
        tups = [ (letter, freq) for letter, freq in fab.iteritems() ]
        tups.sort(key=lambda t: t[1], reverse=mostfreq)
        top = tups[0][0]
        frequentest.append(top)
        
    return frequentest

def main():
    columndata = parse(INPUT_FILE)
    frequentest = confabulate(columndata)
    print "".join(frequentest)
    
    unfrequentest = confabulate(columndata, False)
    print "".join(unfrequentest)

if __name__ == '__main__':
    main()